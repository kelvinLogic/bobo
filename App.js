import React from "react";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import Routes from "./config/routes";

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#F44336",
    accent: "#E040FB"
  }
};

export default function App() {
  return (
    <PaperProvider theme={theme}>
      <Routes />
    </PaperProvider>
  );
}
