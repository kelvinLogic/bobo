import React from "react";
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  Image
} from "react-native";
import { TextInput, Button } from "react-native-paper";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require("../../images/logo.png")}
            style={styles.logo}
          />
          <Text>See what’s trending right now</Text>
        </View>
        <KeyboardAvoidingView behavior="padding" style={styles.formContainer}>
          <TextInput
            style={styles.input}
            label="Email"
            type="flat"
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
          />
          <TextInput
            style={styles.input}
            type="flat"
            label="Password"
            secureTextEntry={true}
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
          <Button
            mode="contained"
            onPress={() => this.props.navigation.navigate("Auth")}
          >
            sign in
          </Button>
          <View style={styles.signupContainer}>
            <Text>dont have an account? </Text>
            <Button
              mode="text"
              onPress={() => this.props.navigation.navigate("Signup")}
            >
              sign up
            </Button>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16
    // backgroundColor: "#ccc"
  },

  logoContainer: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center"
  },

  logo: {
    width: 200,
    height: 200,
    opacity: 0.5
  },

  formContainer: {
    paddingBottom: 150
  },

  input: {
    marginBottom: 28
  },

  signupContainer: {
    // flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 64,
    marginTop: 24
  }
});
