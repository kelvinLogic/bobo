import React from "react";
import { StyleSheet, View, KeyboardAvoidingView } from "react-native";
import { TextInput, Button } from "react-native-paper";

export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior="padding" style={styles.formContainer}>
          <TextInput
            style={styles.input}
            label="Full Name"
            type="flat"
            value={this.state.fullName}
            onChangeText={fullName => this.setState({ fullName })}
          />
          <TextInput
            style={styles.input}
            label="Email"
            type="flat"
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
          />
          <TextInput
            style={styles.input}
            label="Phone Number"
            type="flat"
            value={this.state.phoneNumber}
            onChangeText={phoneNumber => this.setState({ phoneNumber })}
          />
          <TextInput
            style={styles.input}
            type="flat"
            label="Password"
            secureTextEntry={true}
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
          <Button mode="contained" onPress={() => console.log("Pressed")}>
            sign up
          </Button>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  formContainer: {
    padding: 16,
    paddingBottom: 150
  },

  input: {
    marginBottom: 28
  }
});
