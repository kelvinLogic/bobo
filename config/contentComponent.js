import React from "react";
import { StyleSheet, ScrollView, View } from "react-native";
import { DrawerItems, SafeAreaView } from "react-navigation";
import { Text, Avatar, Drawer } from "react-native-paper";

const contentComponent = props => (
  <ScrollView>
    <SafeAreaView
      style={styles.container}
      forceInset={{ top: "always", horizontal: "never" }}
    >
      <View style={styles.profile}>
        <Avatar.Image size={64} source={require("../images/profile.jpg")} />
        <Text style={styles.name}>Daniel Kibet</Text>
        <Text style={styles.location}>Nairobi</Text>
      </View>
      <Drawer.Section title="Some title">
        <Drawer.Item
          label="Clubs"
          icon="nature"
          //   active={active === 'first'}
          onPress={() => {
            props.navigation.navigate("Club");
            props.navigation.closeDrawer();
          }}
        />
        <Drawer.Item
          label="My Crew"
          icon="people"
          //   active={active === 'second'}
          onPress={() => {
            props.navigation.navigate("Crew");
            props.navigation.closeDrawer();
          }}
        />
      </Drawer.Section>
      {/* <DrawerItems {...props} /> */}
    </SafeAreaView>
  </ScrollView>
);

export default contentComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  profile: {
    alignItems: "center",
    justifyContent: "center",
    padding: 16,
    borderBottomColor: "#eee",
    borderBottomWidth: 1
  },

  name: {
    marginTop: 16,
    fontSize: 20
  },
  location: {
    marginTop: 8
  }
});
