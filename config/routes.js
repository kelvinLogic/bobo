import {
  createAppContainer,
  createDrawerNavigator,
  createSwitchNavigator,
  createStackNavigator
} from "react-navigation";
import Club from "../screens/Clubs";
import Crew from "../screens/Crew";
import contentComponent from "./contentComponent";
import Login from "../components/Login/Login";
import Signup from "../components/Signup/Signup";

const DrawerNavigator = createDrawerNavigator(
  {
    Club: Club,
    Crew: Crew
  },
  {
    // initialRouteName: "Club",
    hideStatusBar: true,
    contentComponent: contentComponent
  }
);

const AppStack = createStackNavigator({
  Login: Login,
  Signup: Signup
});

export default createAppContainer(
  createSwitchNavigator(
    {
      // AuthLoading: AuthLoading,
      App: AppStack,
      Auth: DrawerNavigator
    },
    {
      initialRouteName: "Auth"
    }
  )
);

// export default createAppContainer(DrawerNavigator);
