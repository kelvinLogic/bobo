import React from "react";
import { View, Text } from "react-native";
import { Appbar } from "react-native-paper";
import MapView from "react-native-maps";
import * as Permissions from "expo-permissions";

export default class Club extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: -1.2007563,
      longitude: 36.9249029
    };
  }

  async componentDidMount() {
    const { status } = await Permissions.getAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.getLocationAsync();
    }

    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        this.setState({ latitude, longitude }, () =>
          console.log(`long ${this.state.longitude} lat ${this.state.latitude}`)
        );
      },
      error => console.log("Error", error)
    );
  }

  async getLocationAsync() {
    // permissions returns only for location permissions on iOS and under certain conditions, see Permissions.LOCATION
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status === "granted") {
      return Location.getCurrentPositionAsync({ enableHighAccuracy: true });
    } else {
      throw new Error("Location permission not granted");
    }
  }

  render() {
    const { latitude, longitude } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Appbar.Header>
          <Appbar.Action
            icon="reorder"
            onPress={this.props.navigation.openDrawer}
          />
          <Appbar.Content title="Clubs" />
        </Appbar.Header>
        <MapView
          showsUserLocation
          initialRegion={{
            latitude,
            longitude,
            latitudeDelta: -1.2007563,
            longitudeDelta: 36.9249029
          }}
          style={{ flex: 1 }}
        ></MapView>
      </View>
    );
  }
}
