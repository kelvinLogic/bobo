import React from "react";
import { View, Text } from "react-native";
import { Appbar } from "react-native-paper";

export default class Crew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Appbar.Header>
          <Appbar.Action
            icon="reorder"
            onPress={this.props.navigation.openDrawer}
          />
          <Appbar.Content title="My Crew" />
        </Appbar.Header>
        <Text style={{ fontWeight: "bold", marginTop: 20 }}>Crew screen</Text>
      </View>
    );
  }
}
